CREATE SCHEMA AuctionSchema;

CREATE TABLE AuctionSchema.Person (
    PersonID INT PRIMARY KEY,
    Name VARCHAR(100) NOT NULL,
    Email_Address VARCHAR(255) NOT NULL UNIQUE
);

CREATE TABLE AuctionSchema.Seller (
    SellerID INT PRIMARY KEY,
    PersonID INT NOT NULL,
    FOREIGN KEY (PersonID) REFERENCES AuctionSchema.Person(PersonID)
);

CREATE TABLE AuctionSchema.Buyer (
    BuyerID INT PRIMARY KEY,
    PersonID INT NOT NULL,
    FOREIGN KEY (PersonID) REFERENCES AuctionSchema.Person(PersonID)
);

CREATE TABLE AuctionSchema.Auction (
    AuctionID INT PRIMARY KEY,
    "Date" DATE CHECK ("Date" > '2000-01-01'),
    Place VARCHAR(100) NOT NULL CHECK (Place IN ('New York', 'London', 'Paris')),
    "Time" TIME,
    Description VARCHAR(255) NOT NULL
);

CREATE TABLE AuctionSchema.Item (
    ItemID INT PRIMARY KEY,
    Lot_Number INT, CHECK (Lot_Number > 0),
    Starting_Price DECIMAL(10, 2) NOT NULL CHECK (Starting_Price >= 0),
    Description VARCHAR(255) NOT NULL
);

CREATE TABLE AuctionSchema.Sale (
    SaleID INT PRIMARY KEY,
    ItemID INT NOT NULL,
    Actual_Price DECIMAL(10, 2),
    FOREIGN KEY (ItemID) REFERENCES AuctionSchema.Item(ItemID)
);

CREATE TABLE AuctionSchema.Auction_Item (
    AuctionID INT,
    ItemID INT,
    PRIMARY KEY (AuctionID, ItemID),
    FOREIGN KEY (AuctionID) REFERENCES AuctionSchema.Auction(AuctionID),
    FOREIGN KEY (ItemID) REFERENCES AuctionSchema.Item(ItemID)
);

CREATE TABLE AuctionSchema.Seller_Item (
    SellerID INT,
    ItemID INT,
    PRIMARY KEY (SellerID, ItemID),
    FOREIGN KEY (SellerID) REFERENCES AuctionSchema.Seller(SellerID),
    FOREIGN KEY (ItemID) REFERENCES AuctionSchema.Item(ItemID)
);

CREATE TABLE AuctionSchema.Buyer_Sale (
    BuyerID INT,
    SaleID INT,
    PRIMARY KEY (BuyerID, SaleID),
    FOREIGN KEY (BuyerID) REFERENCES AuctionSchema.Buyer(BuyerID),
    FOREIGN KEY (SaleID) REFERENCES AuctionSchema.Sale(SaleID)
);

CREATE TABLE AuctionSchema.Category (
    CategoryID INT PRIMARY KEY,
    Name VARCHAR(100) NOT NULL
);

CREATE TABLE AuctionSchema.Item_Category (
    ItemID INT,
    CategoryID INT,
    PRIMARY KEY (ItemID, CategoryID),
    FOREIGN KEY (ItemID) REFERENCES AuctionSchema.Item(ItemID),
    FOREIGN KEY (CategoryID) REFERENCES AuctionSchema.Category(CategoryID)
);

ALTER TABLE AuctionSchema.Person
ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;

ALTER TABLE AuctionSchema.Seller
ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;

ALTER TABLE AuctionSchema.Buyer
ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;

ALTER TABLE AuctionSchema.Auction
ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;

ALTER TABLE AuctionSchema.Item
ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;

ALTER TABLE AuctionSchema.Sale
ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;

ALTER TABLE AuctionSchema.Auction_Item
ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;

ALTER TABLE AuctionSchema.Seller_Item
ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;

ALTER TABLE AuctionSchema.Buyer_Sale
ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;

ALTER TABLE AuctionSchema.Category
ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;

ALTER TABLE AuctionSchema.Item_Category
ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;

UPDATE AuctionSchema.Person SET record_ts = CURRENT_DATE WHERE record_ts IS NULL;
UPDATE AuctionSchema.Seller SET record_ts = CURRENT_DATE WHERE record_ts IS NULL;
UPDATE AuctionSchema.Buyer SET record_ts = CURRENT_DATE WHERE record_ts IS NULL;
UPDATE AuctionSchema.Auction SET record_ts = CURRENT_DATE WHERE record_ts IS NULL;
UPDATE AuctionSchema.Item SET record_ts = CURRENT_DATE WHERE record_ts IS NULL;
UPDATE AuctionSchema.Sale SET record_ts = CURRENT_DATE WHERE record_ts IS NULL;
UPDATE AuctionSchema.Auction_Item SET record_ts = CURRENT_DATE WHERE record_ts IS NULL;
UPDATE AuctionSchema.Seller_Item SET record_ts = CURRENT_DATE WHERE record_ts IS NULL;
UPDATE AuctionSchema.Buyer_Sale SET record_ts = CURRENT_DATE WHERE record_ts IS NULL;
UPDATE AuctionSchema.Category SET record_ts = CURRENT_DATE WHERE record_ts IS NULL;
UPDATE AuctionSchema.Item_Category SET record_ts = CURRENT_DATE WHERE record_ts IS NULL;

INSERT INTO AuctionSchema.Person (PersonID, Name, Email_Address) VALUES
(1, 'John Doe', 'john.doe@example.com'),
(2, 'Jane Smith', 'jane.smith@example.com');

INSERT INTO AuctionSchema.Seller (SellerID, PersonID) VALUES
(1, 1),
(2, 2);

INSERT INTO AuctionSchema.Buyer (BuyerID, PersonID) VALUES
(1, 1),
(2, 2);

INSERT INTO AuctionSchema.Auction (AuctionID, "Date", Place, "Time", Description) VALUES
(1, '2024-05-10', 'New York', '10:00:00', 'Fine Art Auction'),
(2, '2024-06-15', 'London', '14:00:00', 'Antique Auction');

INSERT INTO AuctionSchema.Item (ItemID, Lot_Number, Starting_Price, Description) VALUES
(1, 101, 500.00, 'Oil Painting'),
(2, 202, 1000.00, 'Antique Vase');

INSERT INTO AuctionSchema.Sale (SaleID, ItemID, Actual_Price) VALUES
(1, 1, 750.00),
(2, 2, 1500.00);

INSERT INTO AuctionSchema.Auction_Item (AuctionID, ItemID) VALUES
(1, 1),
(2, 2);

INSERT INTO AuctionSchema.Seller_Item (SellerID, ItemID) VALUES
(1, 1),
(2, 2);

INSERT INTO AuctionSchema.Buyer_Sale (BuyerID, SaleID) VALUES
(1, 2),
(2, 1);

INSERT INTO AuctionSchema.Category (CategoryID, Name) VALUES
(1, 'Paintings'),
(2, 'Antiques');

INSERT INTO AuctionSchema.Item_Category (ItemID, CategoryID) VALUES
(1, 1),
(2, 2);
